package pff;

import java.util.Comparator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 1796188
 */
public class Person implements Comparable<Person>{
    String name;
    double height, weight;

    @Override
    public String toString() {
        return String.format("%s is %.2f tall and weighs %.2fkg", name, height, weight);
    }

    @Override
    public int compareTo(Person p) {
        return name.compareTo(p.name);
    }
    
    public final static Comparator<Person> compareByHeight = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            if (p1.height == p2.height) return 0;
            if (p1.height > p2.height) return 1; else return -1;
        }
    };
        
    public final static Comparator<Person> compareByWeight = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            if (p1.weight == p2.weight) return 0;
            if (p1.weight > p2.weight) return 1; else return -1;
        }
    };    
    
    
}
