package trips;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class Trip implements Comparable<Trip> {

    Date date;
    String destination;
    int participants;
    double cost;

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public String toString() {
        return String.format("Date: %tD; Destination: %s; Participants: %d; Cost: %.2f", date, destination, participants, cost);
    }

    @Override
    public int compareTo(Trip o) {
        return -this.date.compareTo(o.date);
    }

    static Comparator<Trip> compareByDestination = new Comparator<Trip>() {
        @Override
        public int compare(Trip t1, Trip t2) {
            return t1.destination.compareTo(t2.destination);
        }
    };
}
