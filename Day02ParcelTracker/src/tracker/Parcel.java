package tracker;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class Parcel implements Comparable<Parcel> {

    Date date;
    String recAddr;
    double weight;
    int daysInTransit;

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public String toString() {
        return String.format("Date: %tD; Address: %s; Weight: %.2fkg; Transit: %d", date, recAddr, weight, daysInTransit);
    }

    @Override
    public int compareTo(Parcel p) {
        return this.date.compareTo(p.date);
    }

    static Comparator<Parcel> compareByAddress = new Comparator<Parcel>() {
        @Override
        public int compare(Parcel p1, Parcel p2) {
            return p1.recAddr.compareTo(p2.recAddr);
        }
    };

    static Comparator<Parcel> compareByWeight = new Comparator<Parcel>() {
        @Override
        public int compare(Parcel p1, Parcel p2) {
            if (p1.weight == p2.weight) {
                return 0;
            }
            if (p1.weight > p2.weight) {
                return 1;
            } else {
                return -1;
            }
        }
    };

    static Comparator<Parcel> compareByTransitDays = new Comparator<Parcel>() {
        @Override
        public int compare(Parcel p1, Parcel p2) {
//            if (p1.daysInTransit == p2.daysInTransit) return 0;
//            if (p1.daysInTransit > p2.daysInTransit) return 1; else return -1;
            return p1.daysInTransit - p2.daysInTransit;
        }
    };

}
